package com.example.databindingweather

// Data bindign class
data class WeatherData(
    var visible: Boolean = false,
    var temperature: Double = 0.0,
    var windAngle:Int = 0,
    var windStrength: Int = 0,
    var picture:Int = 0,
    var dayHours:Int = 0,
    var dayMinutes:Int = 0
)
