package com.example.databindingweather

data class JSONCoords(
    val lon: Double,
    val lat:Double
)

data class JSONWeatherEntry(
    val id: Int,
    val main: String,
    val description: String,
    val icon: String
)

data class JSONMeasurements(
    val temp: Double,
    val feels_like: Double,
    val temp_min: Double,
    val temp_max: Double,
    val pressure:Double,
    val humidity:Double
)

data class JSONWind(
    val speed: Double,
    val deg: Double
)

data class JSONClouds(
    val all: Int
)

data class JSONMisc(
    val type: Int,
    val id: Int,
    val country: String,
    val sunrise:Int,
    val sunset:Int
)

data class JSONAPIResponse(
    val coord: JSONCoords,
    val weather: Array<JSONWeatherEntry>,
    val base: String,
    val main: JSONMeasurements,
    val visibility: Double,
    val wind: JSONWind,
    val clouds: JSONClouds,
    val dt:Int,
    val sys: JSONMisc,
    val timezone: Int,
    val id: Int,
    val name: String,
    val cod: Int
)