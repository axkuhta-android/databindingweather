package com.example.databindingweather

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import com.example.databindingweather.databinding.ActivityMainBinding
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.FileNotFoundException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.URL
import java.util.Scanner

class MainActivity : AppCompatActivity() {
    private lateinit var bind: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        bind = DataBindingUtil.setContentView(this, R.layout.activity_main)

        bind.button.setOnClickListener {
            findClick()
        }
    }

    suspend fun loadWeather() {
        val location = bind.editTextLocation.text
        val key = getString(R.string.key)
        val url = "https://api.openweathermap.org/data/2.5/weather?q=${location}&appid=${key}&units=metric"

        try {
            val stream = URL(url).getContent() as InputStream
            val weather = Gson().fromJson(InputStreamReader(stream), JSONAPIResponse::class.java)
            stream.close()
            Log.e("loadWeather", weather.toString())

            val imageResource = when(weather.weather[0].icon) {
                "01d" -> R.drawable.sunny
                "01n" -> R.drawable.clear_night
                "02d" -> R.drawable.partly_cloudy_day
                "02n" -> R.drawable.partly_cloudy_night
                "03d", "04d", "03n", "04n" -> R.drawable.cloudy
                "09d", "10d", "11d" -> R.drawable.rainy
                else -> R.drawable.sunny
            }

            val sunrise = weather.sys.sunrise
            val sunset = weather.sys.sunset
            val delta = sunset - sunrise

            val dayHours = delta / 3600
            val dayMinutes = (delta % 3600) / 60

            bind.mydata = WeatherData(
                visible = true,
                temperature = weather.main.temp,
                windAngle = weather.wind.deg.toInt(),
                windStrength = weather.wind.speed.toInt(),
                picture = R.drawable.clear_night,
                dayHours = dayHours,
                dayMinutes = dayMinutes
            )
        } catch (e:Exception) {
            Log.e("loadWeather", e.toString())

            runOnUiThread {
                AlertDialog.Builder(this)
                    .setTitle("Unable to load weather")
                    .setMessage("No network or location not found?")
                    .setPositiveButton("OK") {
                            dialog, which ->
                    }
                    .show()
            }
        }
    }

    fun findClick() {
        GlobalScope.launch (Dispatchers.IO) {
            loadWeather()
        }
    }
}